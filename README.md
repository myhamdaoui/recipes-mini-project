## recipes-mini-project
a simple crud application that manage recipes using React

### Notes
- Used react-hook-form for building forms
- Used one statful component for "App.js" to manage state, and react hooks for the rest
- Used React Bootstrap 4 to build the UI 
