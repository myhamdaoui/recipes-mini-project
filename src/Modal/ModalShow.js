import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import ListGroup from 'react-bootstrap/ListGroup';
import Col from 'react-bootstrap/Col';
import Image from 'react-bootstrap/Image';
import Row from 'react-bootstrap/Row';

const ModalShow = ({ show, onHide, data, handleEditShow, handleRecipeDelete }) => {
    return (
        <>
            <Modal
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                show={show}
                onHide={onHide}>

                <Modal.Header closeButton>
                    <Modal.Title>{data.title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Row>
                        <Col>
                            <Image src={data.imgUrl} fluid rounded/>
                        </Col>
                        <Col>
                            <p>Ingredients</p>
                            <ListGroup>
                                {
                                    data.ingredients.map((ingredient, index) => <ListGroup.Item key={index}>{ingredient}</ListGroup.Item>)
                                }
                            </ListGroup>
                        </Col>
                    </Row>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="info" onClick={() => {onHide(); handleEditShow();}}>
                        Modifier
                </Button>
                    <Button variant="danger" onClick={handleRecipeDelete.bind(this, data.id)}>
                        Supprimer
                </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default ModalShow;