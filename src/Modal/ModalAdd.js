import React, { useState, useEffect } from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import './ModalAdd.css';
import { useForm } from 'react-hook-form';

const imgPlaceholder = require('./../imgplaceholder.png');

const ModalAdd = ({ show, onHide, onSubmitAddForm }) => {
    // this count is for ingredient
    // it allows to add new input dynamicly
    const [inputsCount, setInputsCount] = useState(1);

    // useForm hook
    const { register, handleSubmit, watch, errors } = useForm();

    useEffect(() => {
        setInputsCount(1);
    }, [show]);  

    // render inputs based on inputsCount state
    const renderInputs = () => {
        const inputs = [];
        for(let i = 0; i < inputsCount; i++) {
            const Input = (
                <Form.Group key={i} controlId={'ingredient-' + i}>
                    <Form.Control
                        isInvalid={ errors.name }
                        name={"ingredient-" + i}
                        type="text" 
                        placeholder={'ingredient ' + (i + 1)} 
                        ref={register({ required: true })}/>
                        <Form.Text className="text-danger">
                        { errors['ingredient-' + i] && 'Ingredient ' + i + ' is required' }
                        </Form.Text>
                </Form.Group>
            );
            inputs.push(Input);
        }

        return inputs;
    }

    // handle submit
    const onSubmit = (data) => {

        // retrieve ingradients
        const ingredients = [];

        for (let key in data) {
            let ingredient = data[key];
            if(key.includes("ingredient")) {
                ingredients.push(ingredient);
            }
        }

        // sent data back
        onSubmitAddForm({
            title: data.name, 
            imgUrl: data.image, 
            ingredients: ingredients
        });

        console.log(data);

        // hide the modal
        onHide();
    }

    return (
        <>
            <Modal
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                show={show}
                onHide={onHide}>

                <Form onSubmit={handleSubmit(onSubmit)}>

                    <Modal.Header closeButton>
                        <Modal.Title>Add new recipe</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>

                        {/* name */}
                        <Form.Group controlId="recipeName">
                            <Form.Label>Recipe name</Form.Label>
                            <Form.Control 
                                isInvalid={ errors.name }
                                type="text" 
                                placeholder="name" 
                                ref={register({ required: true })} 
                                name="name"/>
                            <Form.Text className="text-danger">
                            { errors.name && 'Recipe name is required' }
                            </Form.Text>
                        </Form.Group>

                        {/* image */}
                        <Form.Group controlId="recipeImage">
                            <Form.Label>Image url</Form.Label>
                            <Form.Control 
                                isInvalid={ errors.image }
                                type="url" 
                                placeholder="Image url" 
                                ref={register({ required: true })} 
                                name="image"/>
                            <Form.Text className="text-danger">
                            { errors.image && 'Recipe image is required' }
                            </Form.Text>
                            <img 
                                src={watch('image') || imgPlaceholder} 
                                style={{
                                    width: '150px', 
                                    height: 'auto', 
                                    borderRadius: '5px', 
                                    padding: '5px', 
                                    border: '1px solid #ccc'
                                }} 
                                alt="recipeImg"/>
                        </Form.Group>

                        {/* ingrediens */}
                        <div className="ingredients-header">
                            <div className="font-weight-bold">Ingredients</div>
                            <button className="add btn" type="button" onClick={() => {setInputsCount(inputsCount+1)}}>Add more</button>
                        </div>

                        {
                            renderInputs()
                        }
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={onHide}>
                            Cancel
                    </Button>
                        <Button variant="success" type="submit">
                            Add
                    </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    );
}

export default ModalAdd;