import React from 'react';
import './AppFooter.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart } from '@fortawesome/free-solid-svg-icons';

export const AppFooter = (props) => {
    return (
        <div className="footer text-center">
            Developed with <FontAwesomeIcon color="#6c5ce7" icon={faHeart} /> by <a href="http://myhamdaoui.github.io/">Mohammed Yassin HAMDAOUI</a>
        </div>
    );
};