import React from 'react';

// Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

// App Header
import { AppHeader } from './AppHeader/AppHeader';

// App Footer
import { AppFooter } from './AppFooter/AppFooter';

// Recipe Card
import { RecipeCard } from './RecipeCard/RecipeCard';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import ModalShow from './Modal/ModalShow';
import ModalAdd from './Modal/ModalAdd';
import ModalEdit from './Modal/ModalEdit';

// Recipes data
// import recipes from './data/recipes';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      recipes: [],
      modalShowVisible: false,
      modalAddVisible: false,
      modalEditVisible: false,
      currentOpenRecipe: undefined,
      success: '',
    }
  }

  // SHOW RECIPES LOGIC
  handleShowClick = (recipeId) => {
    // get clicked recipe data
    const recipe = this.state.recipes.find((recipe) => recipe.id === recipeId);

    this.setState({
      currentOpenRecipe: recipe,
      modalShowVisible: true,
      success: ''
    });

  };

  handleShowClose = () => {
    this.setState({
      modalShowVisible: false,
    })
  };


  // ADD RECIPES LOGIC
  handleAddClose = () => {
    this.setState({
      modalAddVisible: false,
    })
  };

  handleAddClick = () => {
    this.setState({
      modalAddVisible: true,
    })
  };

  onSubmitAddForm = (data) => {
    console.log(data);
    // generate a unique id
    const last = Date.now();

    // recipe
    const recipe = {
      id: `recipe${last}`,
      title: data.title,
      imgUrl: data.imgUrl,
      ingredients: data.ingredients
    }

    this.setState({ recipes: [...this.state.recipes, recipe],  success: 'Recipe has been added with success'});
  }

  // EDIT RECIPES LOGIC

  onSubmitEditForm = (recipeEdited) => {
    const recipes = [...this.state.recipes];
    const newRecipes = recipes.map((recipe, i) => {
      if (recipe.id === recipeEdited.id) {
        return recipeEdited
      }
      return recipe;
    });

    this.setState({ recipes: newRecipes, success: 'Recipe has been updated with success' });
  }

  handleEditClose = () => {
    this.setState({
      modalEditVisible: false,
    });
  };

  handleEditShow = () => {
    this.setState({
      modalEditVisible: true,
    });
  }
  // DELETE RECIPES LOGIC

  handleRecipeDelete = (id) => {
    const recipes = [...this.state.recipes];

    const newRecipes = recipes.filter((recipe) => recipe.id !== id);

    this.setState({ recipes: newRecipes, modalShowVisible: false, success: 'Recipe has been deleted with success' });
  };

  // SAUVEGARDE
  // Note: componentWillMount/Update are considered legacy and we should avoiding them
  // that's why I'm using UNSAFE prefix to avoid the warning
  // more info: https://reactjs.org/docs/react-component.html

  UNSAFE_componentWillUpdate(nextProps, nextState) {
    if (this.storageAvailable('localStorage')) {
      localStorage.setItem('recipes',
        JSON.stringify(nextState.recipes))
    } else {
      console.error('Your browser doesn\'t support local storage');
    }
  }

  UNSAFE_componentWillMount() {
    if (this.storageAvailable('localStorage')) {
      const localRef = localStorage.getItem('recipes');
      if (localRef) {
        this.setState({
          recipes: JSON.parse(localRef)
        })
      }
    } else {
      console.error('Your browser doesn\'t support local storage');
    }
  }

  storageAvailable(type) {
    var storage;
    try {
      storage = window[type];
      var x = '__storage_test__';
      storage.setItem(x, x);
      storage.removeItem(x);
      return true;
    }
    catch (e) {
      return e instanceof DOMException && (
        // everything except Firefox
        e.code === 22 ||
        // Firefox
        e.code === 1014 ||
        // test name field too, because code might not be present
        // everything except Firefox
        e.name === 'QuotaExceededError' ||
        // Firefox
        e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
        // acknowledge QuotaExceededError only if there's something already stored
        (storage && storage.length !== 0);
    }
  }

  render() {
    return (
      <Container>
        {
          this.state.currentOpenRecipe &&
          <>
            <ModalShow
              handleRecipeDelete={this.handleRecipeDelete}
              handleEditShow={this.handleEditShow}
              data={this.state.currentOpenRecipe}
              show={this.state.modalShowVisible}
              onHide={this.handleShowClose} />

            <ModalEdit
              onSubmitEditForm={this.onSubmitEditForm}
              show={this.state.modalEditVisible}
              onHide={this.handleEditClose}
              data={this.state.currentOpenRecipe}
            />
          </>
        }
        <ModalAdd
          onSubmitAddForm={this.onSubmitAddForm}
          show={this.state.modalAddVisible}
          onHide={this.handleAddClose}
        />

        <AppHeader handleAddClick={this.handleAddClick} />
        <Row>
          {
            this.state.success && 

            <div className="col">
              <div className="alert alert-success">{this.state.success}</div>
            </div>
          }
        </Row>
        <Row>
          {
            this.state.recipes.length > 0 ?
              this.state.recipes.map((recipe) => (
                <RecipeCard
                  key={recipe.id}
                  id={recipe.id}
                  handleClick={this.handleShowClick}
                  title={recipe.title}
                  imgUrl={recipe.imgUrl}
                />
              )) :
              <div className="col">
                <div className="alert alert-info">no recipes found in your local storage, add new one !</div>
              </div>
          }
        </Row>
        <AppFooter />
      </Container>
    );
  }
}

export default App;
