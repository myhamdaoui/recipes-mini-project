export default [
    {
        id: 1,
        title: "Creamy Biscuits and Gravy",
        ingredients: ['3/4 cup milk', '2 tablespoons white vinegar', '1 cup all-purpose flour', '2 tablespoons white sugar'],
        imgUrl: "https://i2.wp.com/www.downshiftology.com/wp-content/uploads/2020/01/Whole30-Dinner-Recipes-1-500x500.jpg"
    },
    {
        id: 2,
        title: "Puff Pastry Waffles",
        ingredients: ['1 teaspoon baking powder', '2 tablespoons white vinegar', '1 cup all-purpose flour', '2 tablespoons white sugar'],
        imgUrl: "https://www.bbcgoodfood.com/sites/default/files/recipe-collections/collection-image/2013/05/epic-summer-salad.jpg"
    },
    {
        id: 3,
        title: "Puff Pastry Waffles",
        ingredients: ['1/2 teaspoon baking soda', '2 tablespoons white vinegar', '1 cup all-purpose flour', '2 tablespoons white sugar'],
        imgUrl: "https://www.bbcgoodfood.com/sites/default/files/recipe-collections/collection-image/2013/05/chorizo-mozarella-gnocchi-bake-cropped.jpg"
    },
    {
        id: 4,
        title: "Good Old Fashioned Pancakes",
        ingredients: ['1/2 teaspoon salt', '2 tablespoons white vinegar', '1 cup all-purpose flour', '2 tablespoons white sugar'],
        imgUrl: "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/healthy-dinner-ideas-tofu-bowl-1574613204.jpg?crop=0.811xw:0.541xh;0.0737xw,0.247xh&resize=640:*"
    }
]