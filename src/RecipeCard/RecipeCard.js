import React from 'react';
import './RecipeCard.css'

import Col from 'react-bootstrap/Col';

export const RecipeCard = ({ id, title, imgUrl, handleClick }) => {
    const handleCardPress = () => {
        // passe data
        handleClick(id);
    };
    return (
        <Col lg="3" md="4" sm="6" onClick={handleCardPress}>
            <div className="recipe-card">
                <div className="recipe-card-image">
                    <img src={imgUrl} alt="recipe-img" />
                </div>
                <div className="recipe-card-info">
                    <h3>{title}</h3>
                </div>
            </div>
        </Col>
    );
};