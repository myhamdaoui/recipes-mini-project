import React from 'react';
import logo from './../recipe.svg'
import './AppHeader.css'

export const AppHeader = (props) => {
    return (
        <div className="header">
            <img src={logo} alt="logo" />
            <button className="btn btn-outline-primary" onClick={props.handleAddClick}>New recipe</button>
        </div>
    );
};